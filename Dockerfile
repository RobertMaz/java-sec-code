FROM adoptopenjdk/openjdk8:alpine-slim
COPY target/java-sec-code-1.0.0.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "/app/app.jar"]